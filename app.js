const express = require("express");
const bcrypt = require('bcrypt');
const app = express();
const Model = require('./models');
const bodyParser = require('body-parser')
const {User} = Model;
const port = '3000'

// const passport = require('./lib/passport')
// app.use(passport.initialize())

const auth = require('./controller/authController')
const authenticate = require('./middleware/authenticate')
const room = require('./controller/roomController');
const game = require('./controller/gameController')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

app.post('/registration', auth.register)
app.post('/auth', auth.login)
app.post('/room', authenticate, room.createRoom)
app.post('/join', authenticate, game.joinRoom)
app.post('/play', authenticate, game.playGame)
app.get('/winner/:code', authenticate, game.getWinner)
app.get('/', authenticate, (req, res) => {
    const currentUser = req.user;
    console.log(currentUser)
    res.json({
        currentUser
    })
})

app.post('/room', (req, res) => {

})

app.listen(port, (req, res) => {
    console.log(`udah jalan nih di port ${port}`)
})