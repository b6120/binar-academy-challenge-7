// controllers/authController.js
const { User } = require("../models");

function format(user) {
  const userName = user.userName;
  return {
    userName,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  /*... kode untuk fungsi register yang seperti sebelumnya ...*/
  register: async (req, res, next) => {
    try {
        const {email, userName, name, password, role} = req.body
        const createUser = await User.register(email,userName, name, password, role)
        res.json({
            message: "dah nih",
            createdUser: createUser
        })
    }
    catch(err) {
        res.json({
            err: err.message
        })
    }
  },

  login: (req, res) => {
    User.authenticate(req.body.userName, req.body.password)
    .then((user) => {
      res.json(format(user));
    });
  },
};
