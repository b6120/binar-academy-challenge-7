const {Room} = require('../models')


module.exports = {
    createRoom: async (req, res) => {
        const roomCode = Math.ceil(Math.random()*1000)
        const round = req.body.roundNum
        const createdBy = req.user.id
        const payload = {
            code: roomCode,
            roundNum: round,
            status: 'active',
            createdBy: req.user.id,
        }
        const createRoom = await Room.create(payload)
        res.send(createRoom)
    }

}