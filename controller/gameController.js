const { User, Room, RoomPlayer, GameRound, GameRoundHistory } = require("../models");
const room_player = require("../models/room_player");
const user = require("../models/user");

// const room = async () => {
//     const result = await Room.findOne({
//     where: {
//       code,
//     },include: RoomPlayer
//   })
// return result
// }

module.exports = {
  joinRoom: async (req, res) => {
    const code = req.body.roomCode;
    const room = await Room.findOne({
      where: {
        code,
      },
      include: RoomPlayer,
    });
    if (!room) {
      res.status(404).send("Code invalid!");
      return;
    }
    const find = room.RoomPlayers.find(
      (room) => room.player_id === req.user.id
    );

    if (find) {
      res.status(500).send("User sudah join");
      return;
    }

    if (room.RoomPlayers.length === 2) {
      res.status(500).send("Room penuh");
      return;
    }

    const roomPlayer = await RoomPlayer.create({
      room_id: room.id,
      player_id: req.user.id,
    });
    res.send(roomPlayer);
  },
  playGame: async (req, res) => {
    const code = req.body.code;

    const room = await Room.findOne({
      where: {
        code,
      },
      include: [
        {
          model: RoomPlayer,
        },
        {
          model: GameRound,
          include: {
            model: GameRoundHistory,
          },
        },
      ],
    });

    if (!room) {
      res.status(404).send("Code invalid!");
      return;
    }

    const find = room.RoomPlayers.find(
      (room) => room.player_id === req.user.id
    );

    if (!find) {
      res.status(500).send("User belum join, silahkan join dulu");
      return;
    }
    const finishedRound = room.GameRounds.filter(
      (round) => round.status === "finish"
    );
    if (finishedRound.length === room.roundNum) {
      res.status(500).send("room sudah selesai");
      return;
    }
    const activeGameRound = room.GameRounds.find(
      (round) => round.status === "active"
    );

    if (!activeGameRound) {
      const createGameRound = await GameRound.create({
        room_id: room.id,
        status: "active",
      });
    }
    const findGameRound = await GameRound.findOne({
      where: {
        status: "active",
      },
      include: GameRoundHistory,
    });
    const gameRoundHistory = findGameRound.GameRoundHistories.find(
      (history) => history.player_id === req.user.id
    );
    if (gameRoundHistory) {
      res.send({
        message:"user already pick",
        gameRoundHistory
    });
      return;
    }
    const createGameRoundHistory = await GameRoundHistory.create({
      roundId: findGameRound.id,
      player_id: req.user.id,
      selected: req.body.select,
    });
    const result = await GameRound.findOne({
      where: {
        status: "active",
      },
      include: GameRoundHistory,
    });

    if (result.GameRoundHistories.length < 2) {
      res.send("tunggu player satunya dulu yah");
      return;
    }
    const playerOne = result.GameRoundHistories[0];
    const playerTwo = result.GameRoundHistories[1];
    const playerOnePick = playerOne.selected;
    const playerTwoPick = playerTwo.selected;

    if (playerOnePick === "gunting") {
      if (playerTwoPick === "gunting") {
        result.update({
          status: "finish",
        });
      } else if (playerTwoPick === "batu") {
        result.update({
          winner_id: playerTwo.player_id,
          status: "finish",
        });
      } else if (playerTwoPick === "kertas") {
        result.update({
          winner_id: playerOne.player_id,
          status: "finish",
        });
      }
    } else if (playerOnePick === "batu") {
      if (playerTwoPick === "gunting") {
        result.update({
          winner_id: playerOne.player_id,
          status: "finish",
        });
      } else if (playerTwoPick === "batu") {
        result.update({
          status: "finish",
        })
      } else if (playerTwoPick === "kertas") {
        result.update({
          winner_id: playerTwo.player_id,
          status: "finish",
        });
      }
    } else if (playerOnePick === "kertas") {
      if (playerTwoPick === "gunting") {
        result.update({
          winner_id: playerTwo.player_id,
          status: "finish",
        });
      } else if (playerTwoPick === "batu") {
        result.update({
          winner_id: playerOne.player_id,
          status: "finish",
        });
      } else if (playerTwoPick === "kertas") {
        result.update({
          status: "finish",
        });
      }
    }
    if (result.length === room.roundNum)
    return res.send("thank you for playing");

    if (result.GameRoundHistories.length === 2) {
        res.send("play again for the next round");
        return;
      }
   
  },

  getWinner: async (req, res) => {
    const roomCode = req.params.code;
    const room = await Room.findOne({
      where: {
        code: roomCode,
      },
      include:[GameRound, RoomPlayer ],
    });
    if (room.roundNum !== room.GameRounds.length) {
        res.send("room not finish yet")
        return
    }

    const gameRound = room.GameRounds
    const roomPlayer = room.RoomPlayers
    
    let playerOne = roomPlayer[0].player_id;
    let playerTwo = roomPlayer[1].player_id;
    let playerOneWin = 0;
    let playerTwoWin = 0;
    
    gameRound.forEach(round => {
        if (round.winner_id === playerOne) {
            playerOneWin++
        }
        if (round.winner_id === playerTwo) {
            playerTwoWin++
        }
    })
   
    getWinnerId = async() => {
        if (playerOneWin > playerTwoWin) {
            return updateRoomWinner = await room.update({
                winnerId: playerOne,
                status: "finish"
            }) 
        } else if (playerOneWin < playerTwoWin){
            return updateRoomWinner = await room.update({
                winnerId: playerTwo,
                status: "finish"
            })
        } 
    }
    const winner = await getWinnerId()
   
   const findUser = await User.findOne({
    where: {
        id: winner.winnerId
    },
   })
res.send({
    playerOne, playerTwo, playerOneWin, playerTwoWin,
    winner: findUser.userName})
}
}
