"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "user",
      [
        {
          id: "994dcc25-82e4-4585-a71f-f77a917ccca1",
          email: "a@a.com",
          user_name: "a",
          name: "a",
          password: "a",
          role: "player",
          created_at: new Date(),
          updated_at: new Date()
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('user', null, {});
  
  },
};
