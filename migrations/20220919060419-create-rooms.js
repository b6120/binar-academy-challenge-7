'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('room', {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      code: {
        type: Sequelize.STRING,
        allowNull: false,

      },
      roundNum: {
        field: 'round_num',
        type: Sequelize.INTEGER,
        allowNull: false,

      },
      createdBy: {
        field: 'created_by',
        references: {
          model: "user",
          key: "id"
        },
        type: Sequelize.UUID,
        allowNull: false,

      },
      winnerId: {
        field: 'winner_id',
        references: {
          model: "user",
          key: "id"
        },
        type: Sequelize.UUID
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        field: "created_at",
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        field: "updated_at",
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      deletedAt: {
        field: "deleted_at",
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('room');
  }
};