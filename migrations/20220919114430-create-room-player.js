'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('room_player', {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      room_id: {
        allowNull: false,
        references: {
          model: "room",
          key: "id"
        },
        type: Sequelize.UUID
      },
      player_id: {
        allowNull: false,
        references: {
          model: "user",
          key: "id"
        },
        type: Sequelize.UUID
      },
      createdAt: {
        allowNull: false,
        field: "created_at",
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        field: "updated_at",
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE
      },
      deletedAt: {
        field: "deleted_at",
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('room_player');
  }
};