const passport = require('passport')
const { Strategy : JwtStrategy, ExtractJwt } = require('passport-jwt' )
const { User } = require('../models' )

const options = {
jwtFromRequest : ExtractJwt.fromHeader ('authorization'),
secretOrKey : 'Ini rahasia ga boleh disebar-sebar',
}

passport.use(new JwtStrategy (options, async (payload, done) => {
// payload adalah hasil terjemahan JWT, sesuai dengan apa yang kita masukkan di parameter pertama dari jwt.sign
User.findByPk (payload.id)
.then(user => done(null, {
    id: user.id,
    email: user.email,
    userName: user.userName
    }
    ))
.catch(err => done(err, false))
}))
module.exports = passport