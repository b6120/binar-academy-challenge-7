'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomPlayer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of DataTypes lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.RoomPlayer.belongsTo(models.Room, {
        foreignKey: "room_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      })
      models.RoomPlayer.belongsTo(models.User, {
        foreignKey: "player_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      })
    }
  }
  RoomPlayer.init({
    id: {
      allowNull: false,
      primaryKey: true,
      unique: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    room_id: {
      allowNull: false,
      references: {
        model: "rooms",
        key: "id"
      },
      type: DataTypes.UUID
    },
    player_id: {
      allowNull: false,
      references: {
        model: "users",
        key: "id"
      },
      type: DataTypes.UUID
    },
    createdAt: {
      allowNull: false,
      field: "created_at",
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      field: "updated_at",
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE
    },
    deletedAt: {
      field: "deleted_at",
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    paranoid: true,
    modelName: 'RoomPlayer',
    tableName: 'room_player'
  });
  return RoomPlayer;
};