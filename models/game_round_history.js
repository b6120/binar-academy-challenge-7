'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GameRoundHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of DataTypes lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.GameRoundHistory.belongsTo(models.GameRound, {
        foreignKey: "round_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      })
      models.GameRoundHistory.belongsTo(models.User, {
        foreignKey: "player_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      })
    }
  }
  GameRoundHistory.init({
    id: {
      allowNull: false,
      primaryKey: true,
      unique: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    roundId: {
      allowNull: false,
      field: 'round_id',
      references: {
        model: "game_round",
        key: "id"
      },
      type: DataTypes.STRING
    },
    player_id: {
      allowNull: false,
      references: {
        model: "user",
        key: "id"
      },
      type: DataTypes.UUID,
    },
    selected: {
      allowNull: false,
      type: DataTypes.STRING
    },
    createdAt: {
      allowNull: false,
      field: "created_at",
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      field: "updated_at",
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE
    },
    deletedAt: {
      field: "deleted_at",
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    paranoid: true,
    modelName: 'GameRoundHistory',
    tableName: 'game_round_history'
  });
  return GameRoundHistory;
};