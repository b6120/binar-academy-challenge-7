'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GameRound extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of DataTypes lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.GameRound.hasMany(models.GameRoundHistory, {
        foreignKey: "round_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      })
      models.GameRound.belongsTo(models.Room, {
        foreignKey: "room_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      })
      models.GameRound.belongsTo(models.User, {
        foreignKey: "winner_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      })
    }
  }
  GameRound.init({
    id: {
      allowNull: false,
      primaryKey: true,
      unique: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    room_id: {
      allowNull: false,
      references: {
        model: "rooms",
        key: "id"
      },
      type: DataTypes.UUID,
    },
    winner_id: {
      references: {
        model: "users",
        key: "id"
      },
      type: DataTypes.UUID,
    },
    status: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    createdAt: {
      allowNull: false,
      field: "created_at",
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      field: "updated_at",
      defaultValue: DataTypes.NOW,
      type: DataTypes.DATE
    },
    deletedAt: {
      field: "deleted_at",
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    paranoid: true,
    modelName: 'GameRound',
    tableName: 'game_round'
  });
  return GameRound;
};