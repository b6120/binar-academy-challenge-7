"use strict";
const { Model } = require("sequelize");

const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User.hasMany(models.Room, {
        foreignKey: "created_by",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      models.User.hasMany(models.RoomPlayer, {
        foreignKey: "room_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      models.User.hasMany(models.GameRound, {
        foreignKey: "winner_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      models.User.hasMany(models.GameRoundHistory, {
        foreignKey: "player_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
    }

    static async #encrypt(password) {
      const encryptedPassword = await bcrypt.hash(password, 10);
      return encryptedPassword;
    }

     static async register(email, userName, name, password, role) {
      const encryptedPassword = await this.#encrypt(password);
      // console.log(encryptedPassword);
      return this.create({
        email,
        userName,
        name,
        password: encryptedPassword,
        role,
      });
    }
    static async isValidPassword (password) {
        console.log(password)
      const compare = await bcrypt.compare(password, this.password)
    }
    generateToken = () => {
      // Jangan memasukkan password ke dalam payload
      const payload = {
      id: this.id,
      username: this.userName
      }
      // Rahasia ini nantinya kita pakai untuk memverifikasi apakah token ini benar-benar berasal dari aplikasi kita
      const secret = 'Ini rahasia ga boleh disebar-sebar'
      // Membuat token dari data-data diatas
      const token = jwt.sign(payload, secret)
      return token
      }

    static async authenticate (userName, password) {
      try {
        // console.log(password)
        const user = await this.findOne({
          where: {
            userName: userName
          }
        })
        const compare = await bcrypt.compare(password, user.password)
        if (!user || !compare) {
          return Promise.reject("User not found!")
        }
        return Promise.resolve(user)
      }
      catch (err) {
        return Promise.reject(err)
      }
    }
  }

  User.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        unique: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      userName: {
        type: DataTypes.STRING,
        unique: true,
        field: "user_name",
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      role: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "player",
      },
      createdAt: {
        allowNull: false,
        field: "created_at",
        defaultValue: DataTypes.NOW,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        field: "updated_at",
        defaultValue: DataTypes.NOW,
        type: DataTypes.DATE,
      },
      deletedAt: {
        field: "deleted_at",
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      // paranoid: true,
      modelName: "User",
      tableName: "user",
    }
  );
  return User;
};
